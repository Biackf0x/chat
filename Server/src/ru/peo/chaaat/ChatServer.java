package ru.peo.chaaat;

import ru.peo.sampleNetwork.TCPConnectionListener;
import sun.rmi.transport.tcp.TCPConnection;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.ArrayList;


public class ChatServer implements TCPConnectionListener {
    public static void main(String[] args) {
        new ChatServer();
    }

    private final ArrayList<ru.kav.sampleNetwork.TCPConnection> connections = new ArrayList<>();

    private ChatServer() throws RuntimeException {
        System.out.println("Server running...");
        try (ServerSocket serverSocket = new ServerSocket(8189)) {
            while (true) {
                try {
                    new ru.kav.sampleNetwork.TCPConnection(this, serverSocket.accept());
                } catch (IOException e) {
                    System.out.println("TCPConnection exception: " + e);
                }
            }
        } catch (IOException e) {
            throw new RuntimeException (e);
        }
    }
    @Override
    public void onConnectionReady(ru.kav.sampleNetwork.TCPConnection tcpConnection) {
        connections.add(tcpConnection);
        sendToAllConnections("Client connected: "+ tcpConnection);

    }

    private void sendToAllConnections(String value) {
        System.out.println(value);

        for(ru.kav.sampleNetwork.TCPConnection connection : connections){
            connection.sendString(value);
        }
    }

    @Override
    public void onReceiveString(ru.kav.sampleNetwork.TCPConnection tcpConnection, String value) {
        sendToAllConnections(value);

    }

    @Override
    public void onDisconnect(ru.kav.sampleNetwork.TCPConnection tcpConnection) {
        connections.remove(tcpConnection);
        sendToAllConnections("Client disconnected: "+tcpConnection);

    }

    @Override
    public void onException(ru.kav.sampleNetwork.TCPConnection tcpConnection, IOException e) {

    }
}